package pl.okley.location;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by Karol on 15.08.2017.
 */

public class LocationModule extends ReactContextBaseJavaModule {
    private final ReactApplicationContext context;

    public LocationModule(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
    }

    @Override
    public String getName() {
        return "LocationModule";
    }

    @ReactMethod
    void initializeLocation() {

    }
}
