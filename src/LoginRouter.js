import { StackNavigator } from 'react-navigation';
import Login from './containers/Login';

const defaultNavigationOptions = { header: null };

const LoginRouter = StackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: () => ({
        ...defaultNavigationOptions,
      }),
    },
  },
  {
    initialRouteName: 'Login',
  },
);

export default LoginRouter;
