import React, { Component } from 'react';
import { UIManager } from 'react-native';
import PropTypes from 'prop-types';
import { COLOR, ThemeProvider } from 'react-native-material-ui';
import Parse from 'parse/react-native';
import Router from './Router';
import LoginRouter from './LoginRouter';

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
  button: {
    container: {
      margin: 16,
    },
  },
  card: {
    container: {
      padding: 16,
    },
  },
};

export default class App extends Component {
  static childContextTypes = {
    login: PropTypes.func,
    logout: PropTypes.func,
    user: PropTypes.object,
  };

  constructor(props, context) {
    super(props, context);
    UIManager.setLayoutAnimationEnabledExperimental(true);
    Parse.serverURL = 'http://okley.pl:1337/parse';
    Parse.initialize('okley-server', 'RRlAYsFhIWbJwu6M');
    this.state = {
      user: null,
    };
    Parse.User
      .currentAsync()
      .then(user => this.setState({ user }))
      .catch(error => console.log(error));
  }

  getChildContext() {
    return {
      login: this.login,
      logout: this.logout,
      user: this.state.user,
    };
  }

  login = (email, password) =>
    Parse.User.logIn(email, password).then((user) => {
      this.setState({ user });
    });

  logout = () => {
    Parse.User.logOut().then(() => this.setState({ user: null }));
  };

  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        {this.state.user === null ? <LoginRouter /> : <Router />}
      </ThemeProvider>
    );
  }
}
