import { StackNavigator } from 'react-navigation';
import Main from './containers/Main';

const defaultNavigationOptions = { header: null };

const Router = StackNavigator(
  {
    Main: {
      screen: Main,
      navigationOptions: () => ({
        ...defaultNavigationOptions,
      }),
    },
  },
  {
    initialRouteName: 'Main',
  },
);

export default Router;
