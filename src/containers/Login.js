import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parse from 'parse/react-native';
import { View, Text, TextInput, TouchableNativeFeedback } from 'react-native';
import { Toolbar, Button } from 'react-native-material-ui';
import { Metrics } from '../themes';
import { Localizable } from '../lib';

export default class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      email: '',
      password: '',
    };
    this.login = this.login.bind(this);
  }

  componentDidMount() {}

  login = () => {
    this.context
      .login(this.state.email, this.state.password)
      .then((response) => {
        alert('Success!');
        this.setState({ logs: JSON.stringify(response) });
      })
      .catch(err => alert(JSON.stringify(err)));
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <TextInput
          style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
          autoCapitalize="none"
        />
        <TextInput
          style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
          autoCapitalize="none"
        />
        <Button raised primary text="Login" onPress={this.login} />
      </View>
    );
  }
}

Login.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

Login.contextTypes = {
  login: PropTypes.func,
};
