import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parse from 'parse/react-native';
import { View, Text, TouchableNativeFeedback, StyleSheet } from 'react-native';
import { Toolbar, Button, BottomNavigation } from 'react-native-material-ui';
import MapView from 'react-native-maps';
import { Metrics } from '../themes';
import { Localizable } from '../lib';

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default class Trips extends Component {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: 52.234368,
            longitude: 21.021176,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
      </View>
    );
  }
}
