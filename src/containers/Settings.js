import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parse from 'parse/react-native';
import { View, Text, TouchableNativeFeedback } from 'react-native';
import { Toolbar, Button, BottomNavigation } from 'react-native-material-ui';
import { Metrics } from '../themes';
import { Localizable } from '../lib';

export default class Settings extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout = () => {
    this.context.logout();
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <Text style={{ margin: Metrics.marginHorizontal }}>
          {this.context.user.get('username') || ''}
        </Text>
        <Button raised primary text="Logout" onPress={this.logout} />
      </View>
    );
  }
}

Settings.contextTypes = {
  logout: PropTypes.func,
  user: PropTypes.object,
};
