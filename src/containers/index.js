import Login from './Login';
import Main from './Main';
import Settings from './Settings';
import Dashboard from './Dashboard';
import Trips from './Trips';

export default { Login, Main, Settings, Dashboard, Trips };
