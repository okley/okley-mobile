import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parse from 'parse/react-native';
import { View, Text, ScrollView } from 'react-native';
import { Toolbar, Button, Card } from 'react-native-material-ui';
import { Metrics } from '../themes';
import { Localizable } from '../lib';

export default class Dashboard extends Component {
  render() {
    return (
      <ScrollView>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <Card>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
          </Card>
          <Card>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
          </Card>
          <Card>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
          </Card>
          <Card>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
          </Card>
          <Card>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
            <Text>Hello world!</Text>
          </Card>
        </View>
      </ScrollView>
    );
  }
}
