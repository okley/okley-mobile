import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Parse from 'parse/react-native';
import { View, Text, TouchableNativeFeedback } from 'react-native';
import { Toolbar, Button, BottomNavigation } from 'react-native-material-ui';
import { Metrics } from '../themes';
import { Localizable } from '../lib';
import Dashboard from './Dashboard';
import Trips from './Trips';
import Settings from './Settings';

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'dashboard',
    };
  }

  componentDidMount() {}

  renderCurrentTab = () => {
    switch (this.state.activeTab) {
      case 'dashboard':
        return <Dashboard />;
      case 'trips':
        return <Trips />;
      case 'settings':
        return <Settings />;
      default:
        return <View style={{ backgroundColor: 'red' }} />;
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Toolbar />
        <View style={{ flex: 1, flexDirection: 'column' }}>
          {this.renderCurrentTab()}
        </View>
        <BottomNavigation active={this.state.activeTab} hidden={false}>
          <BottomNavigation.Action
            key="dashboard"
            icon="dashboard"
            label="Dashboard"
            onPress={() => this.setState({ activeTab: 'dashboard' })}
          />
          <BottomNavigation.Action
            key="trips"
            icon="map"
            label="Trips"
            onPress={() => this.setState({ activeTab: 'trips' })}
          />
          <BottomNavigation.Action
            key="settings"
            icon="settings"
            label="Settings"
            onPress={() => this.setState({ activeTab: 'settings' })}
          />
        </BottomNavigation>
      </View>
    );
  }
}

Main.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

Main.contextTypes = {
  logout: PropTypes.func,
  user: PropTypes.object,
};
