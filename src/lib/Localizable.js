/* eslint-disable global-require */

import Localizable from 'react-native-i18n';

Localizable.fallbacks = true;
Localizable.defaultLocale = 'pl';

Localizable.translations = {
  pl: require('./languages/pl.json'),
};

export default Localizable;
